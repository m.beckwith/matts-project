## Build
```
./gradlew build
```

## Run
```
./gradlew bootRun
```

Navigate to localhost:8080/hello.